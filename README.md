# Hands-On: CopTwitter

Das ist ein erstes Jupyter-Notebook, welches wir gemeinsam am CorrelAid-Stammtisch erarbeitet haben.

## Abhängigkeiten
- python3

## Quick Start
1. Kopieren der CSV-Dateien aus dem Slack-Channel `#coptwitter` in den Ordner `data`.
2. Erzeugen einer virtuellen Umgebung:
```
python3 -m venv venv
```
3. Aktivieren der virtuellen Umgebung:
```
source venv/bin/activate
```
4. Installieren der Abhängigkeiten:
```
pip install -r requirements.txt
```
5. Starten des Jupyter-Notebooks:
```
jupyter notebook
```
6. Es sollte sich automatisch ein Browserfenster öffnen. Falls nicht, einfach [hier klicken](http://localhost:8888).

Alles Weitere ist im Notebook beschrieben.